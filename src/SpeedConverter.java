import java.util.Scanner;

public class SpeedConverter {

    static Scanner input = new Scanner(System.in);

    public static void toMilesPerHour (){
        String collectKiloMeters = "ENTER KILOMETERS WANT TO CONVERT ?";
        System.out.println(collectKiloMeters);
        double kilometersPerHour = input.nextDouble();
        if (kilometersPerHour < 0 ){
            System.out.println("-1");
        }else {
            double milesPerHour = kilometersPerHour / 1.609 ;
            System.out.println(Math.round(milesPerHour) + " Miles / hour");
        }

    }

    public static void printConversion(){

        System.out.println("please enter number of kilometers per hours");
        double kilometersPerHour = input.nextDouble();
        if (kilometersPerHour > 0){
             double conversion = Math.round(kilometersPerHour / 1.609) ;
            System.out.println(kilometersPerHour + " KM/h = " + conversion + " MI/h");
        } else if (kilometersPerHour <= 0) {
            System.out.println("invaild value");

        }

    }


}
