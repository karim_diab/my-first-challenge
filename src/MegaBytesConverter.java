import java.util.Scanner;

public class MegaBytesConverter {

    private static Scanner input = new Scanner(System.in);

    public static void printMegaBytesAndKiloBytes() {
        String collectMegaBytes = "ENTER MEGABYTES WANT TO CONVERT ?";
        System.out.println(collectMegaBytes);
        int kiloBytes = input.nextInt();

        if (kiloBytes < 0) {

            System.out.println("invalid value");

        } else {
            int megaBytes = kiloBytes / 1024;
            int remainingKiloBytes = kiloBytes % 1024;
            System.out.println(kiloBytes + " KB = " + megaBytes + " MB and " + remainingKiloBytes + " KB");
        }



    }
}




