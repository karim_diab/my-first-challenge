import java.util.Scanner;

public class Greeting {
    private static Scanner input = new Scanner(System.in);
    private static final String COLLECTING_NAME = "Hi ,what is your name?";
    private static final String WELCOMING_MESSAGE = "Welcome to our First Challenge";
    private static final String GOODBYE_MESSAGE = "I hope to see you soon, ";
    private static final String CHALLENGE_TEST = """
            CHOOSE CHALLENGE NUMBER NEED TO TEST ?
            1 - POSTIVE NEGATIVE ZERO
            2 - SPPED CONVERTER
            3 - MEGABYTES CONVERTER 
            4 - BARKING DOGS
            5 - TEEN CHECKER
            """;
    public static String personName;

    public static void setPersonName(String personName) {
        Greeting.personName = personName;
    }

    public static String getPersonName() {
        return personName;
    }

    public static void greetingCustomer(){
        System.out.println(COLLECTING_NAME);
        setPersonName(input.nextLine());
        System.out.println(WELCOMING_MESSAGE + ", "+ getPersonName());
    }

    public static void chooseChallenge (){
        System.out.println(CHALLENGE_TEST);
        int challengeNumber = input.nextInt();

        if (challengeNumber == 1){
            PostiveNegativeZero.checkNumber();

        } else if (challengeNumber == 2) {
            SpeedConverter.toMilesPerHour();
            SpeedConverter.printConversion();

        } else if (challengeNumber == 3) {
            MegaBytesConverter.printMegaBytesAndKiloBytes();
            String finishChallenge = "DO YOU TRY ANOTHER CHALLENGE ? ( yes / no )";
            System.out.println(finishChallenge);

            if (input.nextLine() == "yes" ) {
                System.out.println("done");
                // Greeting.chooseChallenge();
            } else if (input.nextLine() == "no") {
                Greeting.goodbyeCustomer();
            }
        }else if (challengeNumber == 4) {
            BarkingDog.shouldWakeUp();
        } else if (challengeNumber == 5) {
            TeenChecker.hasTeen();
        }

    }


    public static void goodbyeCustomer(){
        System.out.println(GOODBYE_MESSAGE + getPersonName());
    }
}
